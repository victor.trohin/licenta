import mongoose from "mongoose";
import validator from "validator";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const UserSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: [true, 'Please provide name'],
        minlenght: 3,
        maclenght: 20,
        trim: true,
    },
    email: {
        type: String, 
        required: [true, 'Please provide email'],
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: 'Please provide a valid email',
        }
    },
    password: {
        type: String, 
        required: [true, 'Please provide password'],
        minlenght: 6,
        select: false,
    },
    lastName: {
        type: String,
        minlenght: 3,
        maclenght: 20,
        trim: true,
        default: 'lastName',
    },
    location: {
        type: String,
        maclenght: 20,
        trim: true,
        default: 'my city',
    },
    favourites_list: {
        type: [String],
        default: [],
    },
    translated_list: {
        type: [Object],
        default: [],
    },
    library_list: {
        type: [Object],
        default: [],
    },
})

UserSchema.pre('save', async function(){
    if(!this.isModified('password'))
        return
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
})

UserSchema.methods.createJWT = function(){
    return jwt.sign({userId: this._id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_LIFETIME
    })
}

UserSchema.methods.comparePassword = async function(candidatePassword){
    const isMatch = await bcrypt.compare(candidatePassword, this.password);
    return isMatch;
}

export default mongoose.model('User', UserSchema)