import mongoose from "mongoose";

const BookSchema = new mongoose.Schema({
    book_id: {
        type: String, 
        required: [true, 'Please provide a book id'],
        unique: true,
        trim: true,
    },
    book_title: {
        type: String, 
        required: [true, 'Please provide the book title'],
    },
    book_language: {
        type: String, 
        required: [true, 'Please provide the language of the book'],
    },
    book_authors: {
        type: String,
        default: 'Unknown',
    },
    book_subjects: {
        type: String,
        default: 'Not specified',
    },
    book_shelves: {
        type: String,
        default: 'Not specified',
    },
    book_image: {
        type: Buffer,
        default: null,
    },
    book_text: {
        type: Buffer,
        default: null,
    },
    
})

export default mongoose.model('Book', BookSchema)