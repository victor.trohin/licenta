import mongoose from "mongoose";

const TranslatedBookSchema = new mongoose.Schema({
    book_id: {
        type: String, 
        required: [true, 'Please provide a book id'],
        trim: true,
    },
    original_language: {
        type: String, 
        required: [true, 'Please provide the original language of the book'],
    },
    translated_language: {
        type: String, 
        required: [true, 'Please provide the original language of the book'],
    },
    book_text: {
        type: Buffer,
        default: null,
    },
    
})

export default mongoose.model('TranslatedBook', TranslatedBookSchema)