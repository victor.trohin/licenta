import {Routes, Route, BrowserRouter} from 'react-router-dom'
import {Register, Landing, Error, ProtectedRoute } from './pages';
import { Profile, BrowseBooks, MyLibrary, FavouritesBookList, SharedLayout } from './pages/dashboard';
import BookReader from './pages/BookReader';
import TranslatedBookReader from './pages/TranslatedBookReader';

function App() {
  return(
    <BrowserRouter>
      <Routes>
        <Route path="/" element={
          <ProtectedRoute>
            <SharedLayout/>
          </ProtectedRoute>
        }> 
          <Route index element={<BrowseBooks />}/>
          <Route path='favourites' element={<FavouritesBookList />}></Route>
          <Route path='library' element={<MyLibrary />}></Route>
          <Route path='profile' element={<Profile/>}></Route>
        </Route>
        <Route path='book/:id' element={<BookReader />} />
        <Route path='book/:id/:language' element={<TranslatedBookReader />} />
        <Route path="/register" element={<Register/>}/>
        <Route path="/landing" element={<Landing/>}/>
        <Route path="*" element={<Error/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App;
