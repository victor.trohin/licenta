import React, { useState } from 'react'
import Wrapper from '../assets/wrappers/Navbar'
import {FaAlignLeft, FaUserCircle, FaCaretDown} from 'react-icons/fa' 
import {useAppContext} from '../context/appContext'
import Logo from './Logo'
const NavBar = () => {
  const [showLogout, setShowLogout] = useState(false);
  const {toggleSideBar, logout, user} = useAppContext()
  return (
    <Wrapper>
      <div className='nav-center'>
        <button 
        type='button'
          className='toggle-btn' 
          onClick={toggleSideBar}
        >
          <FaAlignLeft/>
        </button>
        <div>
          <Logo/>
          <h3 className='logo-text'>read as you like</h3>
        </div>
        <div className="btn-container">
          <button
            type='button'
            className='btn'
            onClick={() => setShowLogout(!showLogout)}
          >
            <FaUserCircle/>
            {user?.name}
            <FaCaretDown/>
          </button>
          <div className={showLogout? 'dropdown show-dropdown':  'dropdown'}>
            <button 
              type='button'
              className='dropdown-btn' 
              onClick={logout}
            >
                logout
            </button>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export default NavBar