import React from 'react';

const LanguageDropdown = ({ selectedLanguage, onLanguageChange }) => {
  const languageOptions = {
    en: 'English',
    ro: 'Romanian',
    es: 'Spanish',
    fr: 'French',
    de: 'German',
    ar: 'Arabic',
    hi: 'Hindi',
    pt: 'Portuguese',
    ru: 'Russian',
    zh: 'Chinese',
    ja: 'Japanese',
    sq: 'Albanian',
    hy: 'Armenian',
    az: 'Azerbaijani',
    bn: 'Bengali',
    bs: 'Bosnian',
    bg: 'Bulgarian',
    ca: 'Catalan',
    hr: 'Croatian',
    cs: 'Czech',
    da: 'Danish',
    nl: 'Dutch',
    et: 'Estonian',
    fi: 'Finnish',
    ka: 'Georgian',
    el: 'Greek',
    gu: 'Gujarati',
    hu: 'Hungarian',
    id: 'Indonesian',
    it: 'Italian',
    ko: 'Korean',
    lv: 'Latvian',
    lt: 'Lithuanian',
    mk: 'Macedonian',
    ms: 'Malay',
    no: 'Norwegian',
    pl: 'Polish',
    sr: 'Serbian',
    sk: 'Slovak',
    sl: 'Slovenian',
    sv: 'Swedish',
    ta: 'Tamil',
    te: 'Telugu',
    th: 'Thai',
    tr: 'Turkish',
    uk: 'Ukrainian',
    ur: 'Urdu',
    vi: 'Vietnamese',
    cy: 'Welsh',
  };

  return (
    <div>
      <label htmlFor="language-select"><strong>Language: </strong></label>
      <select
        id="language-select"
        value={selectedLanguage}
        onChange={onLanguageChange}
      >
        {Object.entries(languageOptions).map(([code, name]) => (
          <option key={code} value={code}>
            {name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default LanguageDropdown;
