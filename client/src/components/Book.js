import React, { useState, useEffect } from 'react';
import Wrapper from '../assets/wrappers/Book';
import LanguageDropdown from './LanguageDropdown';
import { useAppContext } from '../context/appContext';

function Book({ book }) {
  const {
    book_id,
    book_title,
    book_image,
    book_authors,
    book_language,
    book_shelves,
  } = book;
  const {
    addBookToFavourites,
    favouritesList,
    removeBookFromFavourites,
    translateBook,
    translatedList,
    translationIdInProgress,
    progress,
    addBookToLibrary,
    libraryList,
  } = useAppContext();
  const base64Data = btoa(String.fromCharCode.apply(null, new Uint8Array(book_image.data)));
  const imageDataUrl = `data:image/jpeg;base64,${base64Data}`;
  const [languageSelected, setLanguageSelected] = useState(book_language);
  const [isOriginalLanguage, setIsOriginalLanguage] = useState(true);
  const [translationInProgress, setTranslationInProgress] = useState(false);
  const [translatedInList, setTranslatedInList] = useState([]);
  const [translatedIn, setTranslatedIn] = useState(false);
  const [isThisBookTranslated, setIsThisBookTranslated] = useState(false);
  const isFavourite = favouritesList.includes(book_id);

  const onLanguageChange = (event) => {
    const newLanguage = event.target.value;
    setLanguageSelected(newLanguage);
  };

  useEffect(() => {
    setTranslatedIn(translatedInList.includes(languageSelected));
    setIsOriginalLanguage(languageSelected === book_language);
  }, [languageSelected, book_language, translatedInList]);

  useEffect(() => {
    setTranslationInProgress(translationIdInProgress !== null);
    setIsThisBookTranslated(translationIdInProgress === book_id);
  }, [translationIdInProgress, book_id]);

  useEffect(() => {
    if (translatedList) {
      translatedList.forEach((book1) => {
        if (book1.book_id === book_id) {
          setTranslatedInList((prevList) => [...prevList, book1.language]);
        }
      });
    }
  }, [translatedList, book_id]);

  function isLibraryBook () {
    if(libraryList === null)
      return false;
    if (libraryList.length !== 0) {
      for (let i = 0; i < libraryList.length; i++) {
        const book = libraryList[i];
        if (book.book_id === book_id && book.language === languageSelected) {
          return true;
        }
      }
    }
    return false;
  }

  const handleReadButton = async () => {
    if (isOriginalLanguage) {
      if(!isLibraryBook()){
        await addBookToLibrary({book_id: book_id, language: languageSelected});
      }
      window.open(`book/${book_id}`, '_blank');
    } else if (translatedIn) {
      if(!isLibraryBook()){
        await addBookToLibrary({book_id: book_id, language: languageSelected});
      }
      window.open(`book/${book_id}/${languageSelected}`, '_blank');
    } else {
      setTranslationInProgress(true);
      await translateBook({
        book_id: book_id,
        targetLanguage: languageSelected,
        originalLanguage: book_language,
      });
      setTranslationInProgress(false);
    }
  };

  const handleFavouritesButton = async () => {
    if (isFavourite) {
      await removeBookFromFavourites(book_id);
    } else {
      await addBookToFavourites(book_id);
    }
  };

  return (
    <Wrapper>
      <div className="book">
        <img src={imageDataUrl} alt={book_title} className="book-image" />
        <div className="book-details">
          <h2 className="book-title">{book_title}</h2>
          <p>
            <strong>Author:</strong> {book_authors}
          </p>
          <p>
            <strong>Bookshelves:</strong> {book_shelves}
          </p>
          <LanguageDropdown selectedLanguage={languageSelected} onLanguageChange={onLanguageChange} />
        </div>
        {translationInProgress && isThisBookTranslated && <p>{progress}% translated...</p>}
        {translationInProgress && !isThisBookTranslated && (isOriginalLanguage || translatedIn) && (
          <>
            <div className="button-container">
              <button type="submit" className="btn btn-block" onClick={handleReadButton}>
                Read
              </button>
            </div>
            <div className="button-container">
              <button type="submit" className="btn btn-block" onClick={handleFavouritesButton}>
                {isFavourite ? <>Remove from Favourites</> : <>Add to Favourites</>}
              </button>
            </div>
          </>
        )}
        {translationInProgress && !isThisBookTranslated && !isOriginalLanguage && !translatedIn && (
          <>
            <p>Already a book is in translation progress</p>
            <button type="submit" className="btn btn-block" onClick={handleFavouritesButton}>
              {isFavourite ? <>Remove from favourites</> : <>Add to favourites</>}
            </button>
          </>
        )}
        {!translationInProgress && (
          <>
            <div className="button-container">
              <button type="submit" className="btn btn-block" onClick={handleReadButton}>
                {(isOriginalLanguage || translatedIn) ? 'Read' : 'Translate'}
              </button>
            </div>
            <div className="button-container">
              <button type="submit" className="btn btn-block" onClick={handleFavouritesButton}>
                {isFavourite ? <>Remove from favourites</> : <>Add to favourites</>}
              </button>
            </div>
          </>
        )}
      </div>
    </Wrapper>
  );
}

export default Book;
