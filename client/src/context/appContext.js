import React from 'react'
import {useReducer, useContext } from 'react'
import reducer from './reducer'
import axios from 'axios'
import { 
    DISPLAY_ALERT,
    CLEAR_ALERT,
    REGISTER_USER_BEGIN,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_ERROR,
    LOGIN_USER_BEGIN,
    LOGIN_USER_ERROR,
    LOGIN_USER_SUCCESS,
    TOGGLE_SIDEBAR,
    USER_LOGOUT,
    UPDATE_USER_BEGIN,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_ERROR,
    GET_BOOKS_BEGIN,
    GET_ALL_BOOKS,
    ADD_FAVOURITES_BOOKS_BEGIN,
    ADD_FAVOURITES_BOOKS_SUCCESS,
    ADD_FAVOURITES_BOOKS_ERROR,
    REMOVE_FAVOURITES_BOOKS_BEGIN,
    REMOVE_FAVOURITES_BOOKS_SUCCESS,
    REMOVE_FAVOURITES_BOOKS_ERROR,
    GET_BOOKTEXT_BEGIN,
    GET_BOOKTEXT_SUCCESS,
    GET_BOOKTEXT_ERROR,
    ADD_TRANSLATED_TO_LIST_BEGIN,
    ADD_TRANSLATED_TO_LIST_SUCCESS,
    ADD_TRANSLATED_TO_LIST_ERROR,
    GET_TRANSLATED_TEXT_BEGIN,
    GET_TRANSLATED_TEXT_SUCCESS,
    GET_TRANSLATED_TEXT_ERROR,
    TRANSLATE_BOOK_BEGIN,
    TRANSLATE_BOOK_SUCCESS,
    TRANSLATE_BOOK_ERROR,
    TRANSLATE_BOOK_CONTINUE,
    ADD_BOOK_LIBRARY_BEGIN,
    ADD_BOOK_LIBRARY_SUCCESS,
    ADD_BOOK_LIBRARY_ERROR,
} from "./action"

const user = localStorage.getItem('user');
const token = localStorage.getItem('token');
const userLocation = localStorage.getItem('location');
const translationIdInProgress = localStorage.getItem('translationIdInProgress');
const books = localStorage.getItem('books');

const initialState = {
    isLoading: false,
    showAlert: false,
    alertText: '',
    alertType: '',
    user: user? JSON.parse(user): null,
    token: token,
    userLocation: userLocation || '',
    showSidebar: false,
    books: books? JSON.parse(books) : [],
    favouritesList: user? JSON.parse(user).favourites_list: [],
    translatedList: user? JSON.parse(user).translated_list: [],
    translationIdInProgress: translationIdInProgress? JSON.parse(translationIdInProgress).translationIdInProgress : null,
    libraryList: user? JSON.parse(user).library_list: null,
    progress: 0,
    isOpen: false,
}

const AppContext = React.createContext()


const AppProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const displayAlert = () => {
        dispatch({type: DISPLAY_ALERT})
        clearAlert()
    }

    const clearAlert = () => {
        setTimeout(() => {
            dispatch({
                type: CLEAR_ALERT
            })
        }, 3000);
    }

    const addBooksToLocalStorage = ({books}) => {
        console.log(books);
        localStorage.setItem('books', JSON.stringify(books));
    }

    const addBookTranslationInProgressToLocalStorage = ({ translationIdInProgress }) => {
        localStorage.setItem('translationIdInProgress', JSON.stringify({ translationIdInProgress }));
    }

    const removeBookTranslationInProgressToLocalStorage = () => {
        localStorage.removeItem('translationIdInProgress');
    }

    const addUserToLocalStorage = ({ user, token, location, favouritesList, translatedList }) => {
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('token', token);
        localStorage.setItem('location', location);
        localStorage.setItem('favouritesList', favouritesList);
        localStorage.setItem('translatedList', translatedList);
        localStorage.setItem('libraryList', JSON.stringify(user.library_list));
      };

    const removeUserFromLocalStorage = () => {
        localStorage.removeItem('user');
        localStorage.removeItem('location');
        localStorage.removeItem('token');
        localStorage.removeItem('favouritesList');
        localStorage.removeItem('translatedList');
        localStorage.removeItem('libraryList');
        localStorage.removeItem('books');
    }

    const authFetch = axios.create({
        baseURL: '/api/v1',
    })
    // request interceptor
    authFetch.interceptors.request.use(
        (config) => {
        config.headers['Authorization'] = `Bearer ${state.token}`;
        return config;
        },
        (error) => {
        return Promise.reject(error);
        }
    );
    // response interceptor
    authFetch.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            console.log(error.response);
            if (error.response.status === 401) {
                logout()
            }
            return Promise.reject(error);
        }
    );

    const registerUser = async (currentUser) => {
        dispatch({type: REGISTER_USER_BEGIN})
        try {
            const response = await axios.post('/api/v1/auth/register', currentUser)
            console.log(response)
            const {user, token, location} = response.data
            dispatch({
                type: REGISTER_USER_SUCCESS,
                payload: {user, token, location},
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: location,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            console.log(error.response)
            dispatch({
                type: REGISTER_USER_ERROR,
                payload: {msg: error.response.data.msg},
            })
        }
        clearAlert()
    }

    const loginUser = async (currentUser) => {
        dispatch({type: LOGIN_USER_BEGIN})
        try {
            const {data} = await axios.post('/api/v1/auth/login', currentUser)
            const {user, token, location} = data
            const favourites_list = user.favourites_list
            const translated_list = user.translated_list
            console.log(`favourites list ${initialState.favouritesList}`)
            console.log(`translated list ${initialState.translatedList}`)
            dispatch({
                type: LOGIN_USER_SUCCESS,
                payload: {user, token, location, favourites_list, translated_list},
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: location,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            dispatch({
                type: LOGIN_USER_ERROR,
                payload: {msg: error.response.data.msg},
            })
        }
        clearAlert()
    }

    const toggleSideBar = () => {
        dispatch({type: TOGGLE_SIDEBAR})
    }

    const logout = () => {
        dispatch({type: USER_LOGOUT})
        removeUserFromLocalStorage()
    }

    const updateUser = async (currentUser) => {
        dispatch({ type: UPDATE_USER_BEGIN });
        try {
          const { data } = await authFetch.patch('/auth/updateUser', currentUser);
      
          // no token
          const { user, location, token } = data;
            console.log(`update user context user=${user}, token: ${token}, location: ${location}`);
          dispatch({
            type: UPDATE_USER_SUCCESS,
            payload: { user, location, token },
          });
      
          addUserToLocalStorage(
            {
                user: user, 
                token: token, 
                location: location,
                favouritesList: user.favourites_list,
                translatedList: user.translated_list,
            });
        } catch (error) {
            if(error.response.status !== 401)
                dispatch({
                    type: UPDATE_USER_ERROR,
                    payload: { msg: error.response.data.msg },
                });
        }
        clearAlert();
      };

    const getAllBooks = async () => {
        let url = `/books/get-books`
        dispatch({ type: GET_BOOKS_BEGIN }) 
        try {
            const {data}  = await authFetch(url)
            const {books} = data
            dispatch({
                type: GET_ALL_BOOKS,
                payload: {books},
            })
            addBooksToLocalStorage({books: books});
        } catch (error) {
            console.log(error.response.msg)
            logout()
        }
    }

    const addBookToFavourites = async (book_id) => {
        dispatch({type: ADD_FAVOURITES_BOOKS_BEGIN})
        const user = state.user;
        try {
            const {data} = await authFetch.patch('/auth/add-favourites', {book_id: book_id, email: user.email});
            const {favourites_list} = data
            user.favourites_list = favourites_list
            console.log(`added user fav books: ${user.favourites_list}; list: ${favourites_list}`)
            dispatch({
                type: ADD_FAVOURITES_BOOKS_SUCCESS,
                payload: {
                            favourites_list: favourites_list,
                            updatedUser: user,
                        },
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: userLocation,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            dispatch({
                type: ADD_FAVOURITES_BOOKS_ERROR,
                payload: {msg: error.response.data.msg},
            })
        }
        clearAlert();
    }

    const removeBookFromFavourites = async (book_id) => {
        dispatch({type: REMOVE_FAVOURITES_BOOKS_BEGIN})
        const user = state.user;
        try {
            const {data} = await authFetch.patch('/auth/remove-favourite-book', {book_id: book_id, email: user.email});
            const {favourites_list} = data
            user.favourites_list = favourites_list

            console.log(`removed user fav books: ${user.favourites_list}; list: ${favourites_list}`)
            dispatch({
                type: REMOVE_FAVOURITES_BOOKS_SUCCESS,
                payload: {
                            favourites_list: favourites_list,
                            updatedUser: user,
                        },
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: userLocation,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            dispatch({
                type: REMOVE_FAVOURITES_BOOKS_ERROR,
                payload: {msg: error.response.data.msg},
            })
        }
        clearAlert();
    }

    const getBookText = async (book_id) => {
        dispatch({ type: GET_BOOKTEXT_BEGIN }) 
        try {
            const response = await authFetch(`/books/${book_id}/text`);
            dispatch({
                type: GET_BOOKTEXT_SUCCESS
            })
            clearAlert();
            return response.data;
        } catch (error) {
            console.log(error.response.msg)
            dispatch({
                type: GET_BOOKTEXT_ERROR,
                msg: error.response.msg,

            })
            clearAlert();
        }
    }

    const getTranslatedBookText = async ({book_id, language}) => {
        dispatch({ type: GET_TRANSLATED_TEXT_BEGIN }) 
        try {
            const response = await authFetch(`/books/${book_id}/${language}/text`);
            dispatch({
                type: GET_TRANSLATED_TEXT_SUCCESS
            })
            clearAlert();
            return response.data;
        } catch (error) {
            console.log(error.response.msg)
            dispatch({
                type: GET_TRANSLATED_TEXT_ERROR,
                msg: error.response.msg,

            })
            clearAlert();
        }
    }

    const addToTranslatedList = async (book_id, translatedLanguage) => {
        dispatch({ type: ADD_TRANSLATED_TO_LIST_BEGIN })
        const user = state.user;
        const email = user.email;
        try {
            const response = await authFetch.patch('/auth/add-translated', {book_id, translatedLanguage, email})
            const {translated_list} = response.data
            user.translated_list = translated_list

            console.log(`added user translated books: ${user.translated_list}; list: ${translated_list}`)
            dispatch({
                type: ADD_TRANSLATED_TO_LIST_SUCCESS,
                payload: {
                            translated_list: translated_list,
                            updatedUser: user,
                        },
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: userLocation,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            console.log(error.response.msg)
            dispatch({
                type: ADD_TRANSLATED_TO_LIST_ERROR,
                msg: error.response.msg,

            })
            clearAlert();
        }
    }

    const translateBook = async (bookDetails) => {
        const { book_id, targetLanguage, originalLanguage } = bookDetails;
        dispatch({ 
            type: TRANSLATE_BOOK_BEGIN,
            book_id: book_id,
        })
        addBookTranslationInProgressToLocalStorage({translationIdInProgress: book_id});
        let socket = new WebSocket('ws://localhost:8093');
        try {
          const check = await authFetch.post('/books/find-translated', { book_id: book_id, language: targetLanguage });
      
          if (check.data.book === null) {
            const bookText = await authFetch.get(`/books/${book_id}/text`);
            const book_text = bookText.data;

            socket.onmessage = (event) => {
              const message = JSON.parse(event.data);
              const { progress } = message;
              dispatch({
                type: TRANSLATE_BOOK_CONTINUE,
                progress: Number(progress),
              })
              console.log('Progress:', progress);
            };
      
            socket.onopen = () => {
              console.log('WebSocket connection established');
            };
            socket.onclose = () => {
              console.log('WebSocket connection closed');
            };
            socket.onerror = (error) => {
              console.error('WebSocket error:', error);
            };
      
            const response = await authFetch.patch('/books/translate', {
              book_id: book_id,
              book_text: book_text,
              targetLanguage: targetLanguage,
              originalLanguage: originalLanguage,
            });
      
            const { translated_text } = response.data;
            console.log("translated text: ", translated_text);
            addToTranslatedList(book_id, targetLanguage);
            addBookToLibrary({book_id: book_id, language: targetLanguage});
            dispatch({ type: TRANSLATE_BOOK_SUCCESS })
            removeBookTranslationInProgressToLocalStorage();
            socket.close();
          } else {
            console.log(`Found a book translated ${book_id}, lang: ${targetLanguage}`);
            addToTranslatedList(book_id, targetLanguage);
            addBookToLibrary({book_id: book_id, language: targetLanguage});
            dispatch({ type: TRANSLATE_BOOK_SUCCESS })
            removeBookTranslationInProgressToLocalStorage();
            socket.close();
          }
        } catch (error) {
          console.error('Translation error:', error);
          dispatch({
            type: TRANSLATE_BOOK_ERROR,
            msg: error.response.msg,
          })
          socket.close();
          removeBookTranslationInProgressToLocalStorage();
        }
      };    
      
      const addBookToLibrary = async ({book_id, language}) => {
        dispatch({type: ADD_BOOK_LIBRARY_BEGIN})
        const user = state.user;
        try {
            const {data} = await authFetch.patch('/auth/add-to-library', {book_id: book_id, email: user.email, language: language});
            const {library_list} = data
            user.library_list = library_list
            console.log(`added user library books: ${user.library_list}; list: ${library_list}`)
            dispatch({
                type: ADD_BOOK_LIBRARY_SUCCESS,
                payload: {
                            library_list: library_list,
                            updatedUser: user,
                        },
            })
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: userLocation,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            dispatch({
                type: ADD_BOOK_LIBRARY_ERROR,
                payload: {msg: error.response.data.msg},
            })
        }
        clearAlert();
      }

      const addReadProgress = async ({book_id, language, progress}) => {
        const user = state.user;
        try {
            const {data} = await authFetch.patch('/auth/add-progress', {
                book_id: book_id, 
                email: user.email, 
                language: language, 
                progress: progress
            });
            const {library_list} = data
            user.library_list = library_list
            console.log(`added read progress: ${library_list}`)
            addUserToLocalStorage(
                {
                    user: user, 
                    token: token, 
                    location: userLocation,
                    favouritesList: user.favourites_list,
                    translatedList: user.translated_list,
                });
        } catch (error) {
            console.log(error.msg)
        }
        clearAlert();
      }
      
    
    return <AppContext.Provider 
        value={
                {...state, 
                    displayAlert, 
                    registerUser, 
                    loginUser, 
                    toggleSideBar, 
                    logout,
                    updateUser,
                    getAllBooks,
                    addBookToFavourites,
                    removeBookFromFavourites,
                    getBookText,
                    translateBook,
                    getTranslatedBookText,
                    addBookToLibrary,
                    addReadProgress,
                }
                
            }>
        {children}
    </AppContext.Provider>

}

const useAppContext = () => {
    return useContext(AppContext)
}

export {AppProvider, initialState, useAppContext}