import styled from 'styled-components';

const Wrapper = styled.div`
  .book {
    background: var(--white);
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 1rem;
    margin: 1rem;
    width: 400px;
    border-radius: 4px;
    transition: background-color 0.3s ease, box-shadow 0.3s ease;
  }

  .book:hover {
    background: var(--primary-200);
    box-shadow: var(--shadow-3);
  }

  .book-image {
    max-width: 100%;
    max-height: 100%;
    object-fit: contain;
    border-radius: 4px;
    margin-bottom: 1rem;
  }

  .book-details {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 1px;
  }
  .book-details > * + * {
    margin: 0px; /* Adjust the margin-top value to control the spacing between paragraphs */
  }

  .book-title {
    font-size: 1.3rem;
    margin-bottom: 0.5rem;
  }

  .book-authors {
    font-size: 0.8rem;
    margin: 0;
    text-align: center;
  }

  .button-container {
    display: flex;
    justify-content: center;
    width: 300px;
    gap: 10px;
    padding: 1rem;
  }
`;

export default Wrapper;