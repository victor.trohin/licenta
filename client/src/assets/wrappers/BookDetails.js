import styled from 'styled-components'

const Wrapper = styled.aside`
  .content {
    background: var(--white);
    width: fit-content;
    height: fit-content;
    border-radius: var(--borderRadius);
    padding: 4rem 2rem;
    position: relative;
    display: flex;
    align-items: center;
    flex-direction: column;
  }
  .close-btn {
    position: absolute;
    top: 10px;
    left: 10px;
    background: transparent;
    border-color: transparent;
    font-size: 2rem;
    color: var(--grey-500);
    cursor: pointer;
  }
  .column {
  flex: 1;
  padding: 10px;
}

.book-image {
  width: 200px;
  height: auto;
}

.column p {
  margin: 5px 0;
}
.column p strong{
  font-weight: bold;
}

.button-container {
  display: flex;
  justify-content: flex-start;
  gap: 10px;
  margin-top: 20px;
}

`
export default Wrapper
