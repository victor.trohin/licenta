import React from 'react'
import main from '../assets/images/main.svg'
import Wrapper from '../assets/wrappers/Testing'
import { Logo } from '../components'
import { Link } from 'react-router-dom'

const Landing = () => {
  return (
    <Wrapper>
     <nav>
        <Logo />
     </nav>
     <div className='container page'>
      <div className="info">
        <h1>
            book <span>translator</span> app
        </h1>
        <p>
            I'm baby bodega boys banh mi +1 gatekeep yuccie, semiotics vape affogato flexitarian meditation neutral milk hotel la croix. Etsy leggings DSA, chicharrones VHS brunch kale chips YOLO. Fit adaptogen gochujang, letterpress actually craft beer cold-pressed chia plaid. 
        </p>
        <Link to='/register' className='btn btn-hero'>
            Login/Register
        </Link>
      </div>
      <img src={main} alt="job hunt" className='img main-img'/>
     </div>
    </Wrapper>
  )
}

export default Landing