import React, { useEffect, useState } from 'react';
import { useAppContext } from '../../context/appContext';
import Wrapper from '../../assets/wrappers/BrowseBooks';
import Book from '../../components/Book';

const FavouritesBookList = () => {
  const { favouritesList, books, isLoading } = useAppContext();
  const [favBooks, setFavBooks] = useState([]);

  useEffect(() => {
    // Filter the books based on the favorites list
    const filteredBooks = books.filter((book) => favouritesList.includes(book.book_id));
    setFavBooks(filteredBooks);
  }, [favouritesList, books]);

  if (isLoading) {
    return <p>Loading favourite books...</p>;
  } else {
    return (
      <Wrapper>
        <div className="browse-books">
          <div className="book-grid">
            {favBooks.map((book) => (
              <Book key={book.book_id} book={book} />
            ))}
          </div>
        </div>
      </Wrapper>
    );
  }
};

export default FavouritesBookList;
