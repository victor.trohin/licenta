import React, { useEffect } from 'react';
import { useAppContext } from '../../context/appContext';
import Wrapper from '../../assets/wrappers/BrowseBooks';
import Book from '../../components/Book';

const BrowseBooks = () => {
  const { getAllBooks, books, isLoading } = useAppContext();

  useEffect(() => { 
    const fetchData = async () => {
      try {
        await getAllBooks();
      } catch (error) {
        console.log(error.response);
      }
    };

    fetchData();
  }, []);


  if(isLoading)
    return(<p>Loading books...</p>)
  else
  return (
    <Wrapper>
      <div className="browse-books">
        <div className="book-grid">
          {books.map((book) =>(
            <Book 
              key={book.book_id}
              book={book}
            />
          ))}
        </div>
      </div>
    </Wrapper>
    );
};

export default BrowseBooks