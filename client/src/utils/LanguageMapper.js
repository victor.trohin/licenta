import React from 'react';

const LanguageMapper = ({ languageCode }) => {
    const languageMap = {
        en: 'English',
        ro: 'Romanian',
        es: 'Spanish',
        fr: 'French',
        de: 'German',
        ar: 'Arabic',
        hi: 'Hindi',
        pt: 'Portuguese',
        ru: 'Russian',
        zh: 'Chinese',
        ja: 'Japanese',
        sq: 'Albanian',
        hy: 'Armenian',
        az: 'Azerbaijani',
        bn: 'Bengali',
        bs: 'Bosnian',
        bg: 'Bulgarian',
        ca: 'Catalan',
        hr: 'Croatian',
        cs: 'Czech',
        da: 'Danish',
        nl: 'Dutch',
        et: 'Estonian',
        fi: 'Finnish',
        ka: 'Georgian',
        el: 'Greek',
        gu: 'Gujarati',
        hu: 'Hungarian',
        id: 'Indonesian',
        it: 'Italian',
        ko: 'Korean',
        lv: 'Latvian',
        lt: 'Lithuanian',
        mk: 'Macedonian',
        ms: 'Malay',
        no: 'Norwegian',
        pl: 'Polish',
        sr: 'Serbian',
        sk: 'Slovak',
        sl: 'Slovenian',
        sv: 'Swedish',
        ta: 'Tamil',
        te: 'Telugu',
        th: 'Thai',
        tr: 'Turkish',
        uk: 'Ukrainian',
        ur: 'Urdu',
        vi: 'Vietnamese',
        cy: 'Welsh',
      };

  const languageName = languageMap[languageCode] || 'Unknown Language';

  return <span>{languageName}</span>;
};

export default LanguageMapper;