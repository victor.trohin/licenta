import React from 'react'
import {ImBooks} from 'react-icons/im'
import {BsFillBookmarkHeartFill} from 'react-icons/bs'
import {CgProfile} from 'react-icons/cg'
import {HiSearchCircle} from 'react-icons/hi'

const links = [
    {
        id: 1,
        text: 'browse',
        path: '/',
        icon: <HiSearchCircle/>
    },
    {
        id: 2,
        text: 'favourites',
        path: 'favourites',
        icon: <BsFillBookmarkHeartFill/>
    },
    {
        id: 3,
        text: 'library',
        path: 'library',
        icon: <ImBooks/>
    },
    {
        id: 4,
        text: 'profile',
        path: 'profile',
        icon: <CgProfile/>
    }
]

export default links