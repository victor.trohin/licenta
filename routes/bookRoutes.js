import express from 'express';
const router = express.Router();

import {
    addBook,
    updateBook,
    deleteBook,
    getAllBooks,
    addBookToFavourites,
    getBookIdText,
    translateBook,
    getBookById,
    findTranslatedBook,
    getTranslatedBookText,
} from '../controllers/bookController.js'

router.get('/get-books', getAllBooks)
router.post('/add-books', addBook)
router.patch('/update-books', updateBook)
router.delete('/delete-books', deleteBook)
router.patch('/add-favourites', addBookToFavourites)
router.get('/:bookId/text', getBookIdText)
router.patch('/translate', translateBook);
router.get('/:bookId', getBookById);
router.post('/find-translated', findTranslatedBook)
router.get('/:bookId/:language/text', getTranslatedBookText)

export default router;